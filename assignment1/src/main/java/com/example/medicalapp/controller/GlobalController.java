package com.example.medicalapp.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.medicalapp.model.Caregiver;

@Controller
@RequestMapping("/")
public class GlobalController {
	@RequestMapping("/")
	public String viewHomePage(Model model) {
	    return "index";
	}

}
