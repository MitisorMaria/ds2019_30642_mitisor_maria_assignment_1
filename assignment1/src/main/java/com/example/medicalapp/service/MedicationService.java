package com.example.medicalapp.service;

import java.util.List;

import com.example.medicalapp.model.Medication;

public interface MedicationService {
	Medication save(Medication medication);
    List<Medication> findAll();
    void delete(Medication p);

    Medication getMedicationById(String id);

    Medication update(Medication medicationDetails) throws Exception;
}
