package com.example.medicalapp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "medications")
public class Medication implements Serializable{
	@Id
	private String id;
	@Column(name = "name")
	private String name;
	@Column(name = "side_effects")
	private String sideEffects;
	@Column(name = "dosage")
	private int dosage;
	
	public String getSideEffects() {
		return sideEffects;
	}


	public void setSideEffects(String sideEffects) {
		this.sideEffects = sideEffects;
	}


	public int getDosage() {
		return dosage;
	}


	public void setDosage(int dosage) {
		this.dosage = dosage;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
